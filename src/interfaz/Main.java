package interfaz;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

import mundo.Catalogo;

public class Main {
	
	private final static String DATOS= "data/ItinerarioAeroCivil-v2.xlsx";
	public static void main(String[] args) 
	{
		BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(System.out));
		Scanner lector = new Scanner(System.in);
		Catalogo gestor= new Catalogo();
		int opcion = -1;
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto 3---------------\n");
				escritor.write("Ingrese un numeral\n");
				escritor.write("Opciones:\n");
				escritor.write("1: Verificar lectura de costos\n");
				escritor.write("2: Verificar lectura de vuelos\n");
				escritor.write("0: Salir\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: gestor.cargarTarifa(DATOS); break;
				case 2: gestor.cargarVuelos(DATOS); break;
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		
		try {
			escritor.close();
			lector.close();
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
