package Test;

import java.util.Comparator;

import Estructuras.HeapArreglo;
import junit.framework.TestCase;

public class HeapArregloTest extends TestCase
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase donde se har�n las pruebas.
	 */
	private HeapArreglo<String> heap1;

	private HeapArreglo<Integer> heap2;



	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Escenario 1: Construye una nueva tabla con llaves de Strings, capacidad 5 y factor de carga m�xima 0.5
	 */
	private void setupEscenario1() 
	{
		class comparador implements Comparator<String>{

			public int compare(String arg0, String arg1) {
				// TODO Auto-generated method stub
				return arg1.compareTo(arg0);
			}


		}
		heap1 = new HeapArreglo<String>(new comparador());
	}

	/**
	 * Escenario 2: Construye una nueva tabla con llaves de enteros, capacidad 5 y factor de carga m�xima 0.5
	 */
	private void setupEscenario2() 
	{
		class comparador implements Comparator<Integer>{



			public int compare(Integer arg0, Integer arg1) {
				return arg1-arg0;
			}


		}
		heap2 = new HeapArreglo<Integer>(new comparador()	);
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo agregar.<br>
	 * <b> M�todos a probar:</b> <br>
	 * put<br>
	 * peek<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La tabla est� vac�a. <br>
	 * 2. Se agregan elementos
	 */
	public void testAgregar() {
		setupEscenario1();
		assertEquals("No deber�a encontrar nada", null , heap1.peek());
		heap1.put("a");
		heap1.put("b");
		heap1.put("c");
		assertEquals("El tama�o del heap deber�a ser 3", 3, heap1.size());
		assertEquals("deber�a encontrar el objeto correcto","c", heap1.peek());

		setupEscenario2();
		assertEquals("No deber�a encontrar nada", null , heap2.peek());
		heap2.put(1);
		heap2.put(2);
		heap2.put(5);
		assertEquals("El tama�o del heap deber�a ser 3", 3, heap2.size());
		assertEquals("deber�a encontrar el objeto correcto",5,(int) heap2.peek());
	}

	/**
	 * Prueba 2: Se encarga de verificar el m�todo poll.<br>
	 * <b> M�todos a probar:</b> <br>
	 * put<br>
	 * poll<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La tabla est� vac�a. <br>
	 * 2. Se agregan y se eliminan elementos
	 */
	public void testEliminar() {
		setupEscenario1();
		assertTrue("El heap deber�a estar vac�o", heap1.isEmpty());
		heap1.put("g");
		heap1.put("b");
		heap1.put("h");
		heap1.put("c");
		heap1.put("z");
		heap1.put("l");
		heap1.put("x");
		assertEquals("El tama�o de la lista deber�a ser 7", 7, heap1.size());

		assertEquals("deber�a encontrar el objeto correcto","z",  heap1.poll());
		assertEquals("deber�a encontrar el objeto correcto","x",  heap1.poll());
		assertEquals("deber�a encontrar el objeto correcto","l",  heap1.poll());
		assertEquals("deber�a encontrar el objeto correcto","h",  heap1.poll());
		assertEquals("deber�a encontrar el objeto correcto","g",  heap1.poll());
		assertEquals("El tama�o de la lista deber�a ser 2", 2, heap1.size());
		assertEquals("deber�a encontrar el objeto correcto","c",  heap1.poll());
		assertEquals("deber�a encontrar el objeto correcto","b",  heap1.poll());
		assertEquals("El tama�o de la lista deber�a ser 0", 0, heap1.size());

		setupEscenario2();
		assertTrue("El heap deber�a estar vac�o", heap2.isEmpty());
		heap2.put(5);
		heap2.put(2);
		heap2.put(1);
		heap2.put(9);
		heap2.put(11);
		heap2.put(10);
		heap2.put(7);
		assertEquals("El tama�o de la lista deber�a ser 7", 7, heap2.size());
		assertEquals("deber�a encontrar el objeto correcto",11,(int) heap2.poll());
		assertEquals("deber�a encontrar el objeto correcto",10,(int) heap2.poll());
		assertEquals("deber�a encontrar el objeto correcto",9, (int)heap2.poll());
		assertEquals("deber�a encontrar el objeto correcto",7, (int)heap2.poll());
		assertEquals("deber�a encontrar el objeto correcto",5, (int)heap2.poll());
		assertEquals("El tama�o de la lista deber�a ser 2", 2, heap2.size());
		assertEquals("deber�a encontrar el objeto correcto",2, (int)heap2.poll());
		assertEquals("deber�a encontrar el objeto correcto",1, (int)heap2.poll());
		assertEquals("El tama�o de la lista deber�a ser 0", 0, heap2.size());


	}
}
