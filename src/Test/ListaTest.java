package Test;


import java.util.Comparator;
import java.util.Iterator;

import Estructuras.Lista;
import junit.framework.TestCase;

public class ListaTest extends TestCase
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase donde se har�n las pruebas.
	 */
	private Lista<String> lista;

	private Lista<Integer> lista2;
	
	private Comparator<String> comparador;
	
	private Comparator<Integer> comparador2;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Escenario 1: Construye una nueva lista de Strings vac�a
	 */
	private void setupEscenario1( )
	{
		lista= new Lista<String>();
		class ComparadorString implements Comparator<String>{
			public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}			
		}
		comparador = new ComparadorString();
	}
	/**
	 * Escenario 2: Construye una nueva lista de enteros vac�a
	 */
	private void setupEscenario2( )
	{
		lista2= new Lista<Integer>();
		class ComparadorInt implements Comparator<Integer>  {

			public int compare(Integer arg0, Integer arg1) {
				return arg0-arg1;
			}			
		}
		comparador2=new ComparadorInt();
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo agregar al principio y al final de la clase.<br>
	 * <b> M�todos a probar:</b> <br>
	 * agregarAlPrincipio<br>
	 * agregarAlFinal<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La lista est� vac�a. <br>
	 * 2. Se agrega al principio.
	 * 3. Se agrega al final.
	 * 4. Se agrega tanto al principio como al final
	 */
	public void testAgregar()
	{
		setupEscenario1( );
		assertTrue( "La lista deber�a estar vac�a",lista.isEmpty() );
		lista.agregarAlPrincipio("a");
		lista.agregarAlPrincipio("b");
		assertEquals( "El tama�o de la lista deber�a ser 2", 2, lista.size() );
		setupEscenario2( );
		assertEquals( "El tama�o de la lista deber�a ser 0", 0, lista2.size() );
		lista2.agregarAlPrincipio(1);
		lista2.agregarAlPrincipio(2);
		assertEquals( "El tama�o de la lista deber�a ser 2", 2, lista2.size() );
		setupEscenario1();
		assertTrue( "La lista deber�a estar vac�a",lista.isEmpty() );
		lista.agregarAlFinal("c");
		lista.agregarAlPrincipio("b");
		lista.agregarAlPrincipio("a");
		lista.agregarAlFinal("d");
		assertEquals( "El tama�o de la lista deber�a ser 4", 4, lista.size() );
		Iterator<String> i= lista.iterator();
		String a= i.next();
		String b= i.next();
		String c= i.next();
		String d= i.next();

		assertEquals( "No agreg� en el orden correcto", "a", a );
		assertEquals( "No agreg� en el orden correcto", "b", b );
		assertEquals( "No agreg� en el orden correcto", "c", c );	
		assertEquals( "No agreg� en el orden correcto", "d", d );

		assertEquals( "No agreg� en el orden correcto", lista.darPrimero(), a );
		assertEquals( "No agreg� en el orden correcto", lista.darUltimo(), d );
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo quitarPrimero de la clase.<br>
	 * <b> M�todos a probar:</b> <br>
	 * quitarPrimero<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. Se agregan y se quitan elementos.
	 */
	public void testEliminar()
	{
		setupEscenario1( );
		lista.agregarAlPrincipio("d");
		lista.agregarAlPrincipio("c");
		lista.agregarAlPrincipio("b");
		lista.agregarAlPrincipio("a");
		lista.quitarPrimero();
		lista.quitarPrimero();
		assertEquals( "El tama�o de la lista deber�a ser 2", 2, lista.size() );
		lista.agregarAlFinal("e");
		lista.agregarAlFinal("f");

		Iterator<String> i= lista.iterator();
		String c= i.next();
		String d= i.next();
		String e= i.next();
		String f= i.next();

		assertEquals( "No deber�a cambiar el orden", "c", c );
		assertEquals( "No deber�a cambiar el orden", "d", d );
		assertEquals( "No deber�a cambiar el orden", "e", e );
		assertEquals( "No deber�a cambiar el orden", "f", f );

		assertEquals( "No deber�a cambiar el orden", lista.darPrimero(), c );
		assertEquals( "No deber�a cambiar el orden", lista.darUltimo(), f );
	}

	/**
	 * Prueba 3: Se encarga de verificar el m�todo sort de la clase.<br>
	 * <b> M�todos a probar:</b> <br>
	 * sort<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. Se agregan elementos y se ordena
	 */
	public void testOrdenar()
	{
		setupEscenario1( );
		lista.agregarAlPrincipio("b");
		lista.agregarAlFinal("c");
		lista.agregarAlFinal("d");
		lista.agregarAlPrincipio("a");
		lista.sort(comparador);

		Iterator<String> i= lista.iterator();
		String a= i.next();
		String b= i.next();
		String c= i.next();
		String d= i.next();

		assertEquals( "Deber�a haberse ordenado", "a", a );
		assertEquals( "Deber�a haberse ordenado", "b", b );
		assertEquals( "Deber�a haberse ordenado", "c", c );
		assertEquals( "Deber�a haberse ordenado", "d", d );

		setupEscenario2();
		lista2.agregarAlFinal(1);
		lista2.agregarAlPrincipio(4);
		lista2.agregarAlFinal(2);
		lista2.agregarAlPrincipio(3);
		lista2.sort(comparador2);

		Iterator<Integer>i2= lista2.iterator();
		int aa= i2.next();
		int bb= i2.next();
		int cc= i2.next();
		int dd= i2.next();

		assertEquals( "Deber�a haberse ordenado", 1, aa );
		assertEquals( "Deber�a haberse ordenado", 2, bb );
		assertEquals( "Deber�a haberse ordenado", 3, cc );
		assertEquals( "Deber�a haberse ordenado", 4, dd );
	}
}

