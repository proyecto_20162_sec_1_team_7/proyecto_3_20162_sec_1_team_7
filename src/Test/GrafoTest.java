package Test;
import java.util.Arrays;
import java.util.Collection;

import junit.framework.TestCase;
import Estructuras.Arco;
import Estructuras.Grafo;
import Estructuras.GrafoNoDirigido;
import Estructuras.Nodo;

public class GrafoTest extends TestCase
{

	Grafo<Integer, Integer>g1;
	Grafo<String, Integer> g2;
	
	private static boolean contains(Nodo[] arr, Nodo o){
		for(int i=0; i<arr.length; i++){
			boolean b = o.equals(arr[i]);
			if(b){
				return true;
			}
		}
		return false;
	}
	private static boolean contains2(Arco[] arr, Arco o){
		for(int i=0; i<arr.length; i++){
			boolean b = o.equals(arr[i]);
			if(b){
				return true;
			}
		}
		return false;
	}
	
	private void setupEscenario1(){
		g1 = new GrafoNoDirigido<Integer, Integer>();
		g1.agregarNodo(1);
		g1.agregarNodo(2);
		g1.agregarNodo(3);
		g1.agregarNodo(4);
		g1.agregarNodo(5);
		g1.agregarArco(1, 2, 1);
		g1.agregarArco(2, 3, 1);
		g1.agregarArco(3, 4, 1);
		g1.agregarArco(4, 5, 1);
		g1.agregarArco(5, 1, 1);
		g1.agregarArco(2, 4, 1);
	}
	private void setupEscenario2(){
		g2 = new GrafoNoDirigido<String, Integer>();
		g2.agregarNodo("1");
		g2.agregarNodo("2");
		g2.agregarNodo("3");
		g2.agregarNodo("4");
		g2.agregarNodo("5");
		g2.agregarArco("1", "2", 1);
		g2.agregarArco("2", "3", 1);
		g2.agregarArco("3", "4", 1);
		g2.agregarArco("4", "5", 1);
		g2.agregarArco("5", "1", 1);
		g2.agregarArco("2", "4", 1);
	}
	
	public void testAgregarNodo1(){
		setupEscenario1();
		assertEquals("El nodo debería estar",true, contains(g1.darNodos(), new Nodo<Integer>(1)));
		assertEquals("El nodo no debería agregarse",false, g1.agregarNodo(1));
		assertEquals("El tamaño no es el esperado",g1.darNodos().length ,5);
		assertEquals("El nodo no debería agregarse",true, g1.agregarNodo(6));
		assertEquals("El nodo debería estar",true, contains(g1.darNodos(), new Nodo<Integer>(6)));
		
	}
	public void testAgregarNodo2(){
		setupEscenario2();
		assertEquals("El nodo debería estar",true, contains(g2.darNodos(), new Nodo<String>("1")));
		assertEquals("El nodo no debería agregarse",false, g2.agregarNodo("1"));
		assertEquals("El tamaño no es el esperado",g2.darNodos().length ,5);
		assertEquals("El nodo no debería agregarse",true, g2.agregarNodo("6"));
		assertEquals("El nodo debería estar",true, contains(g2.darNodos(), new Nodo<String>("6")));
		
	}
	
	public void testAgregarArco1(){
		setupEscenario1();
		assertEquals("El Arco debería estar",true, contains2(g1.darArcos(), g1.darArcosOrigen(4)[0]));
		g1.agregarNodo(6);
		assertEquals("El arco deberia agregarse" , true,g1.agregarArco(6, 1, 1));
		assertEquals("El Arco debería estar",true, contains2(g1.darArcos(), g1.darArcosOrigen(6)[0]));
	}
	
	public void testAgregarArco2(){
		setupEscenario2();
		assertEquals("El Arco debería estar",true, contains2(g2.darArcos(), g2.darArcosOrigen("4")[0]));
		g2.agregarNodo("6");
		assertEquals("El arco deberia agregarse" , true,g2.agregarArco("6","1", 1));
		assertEquals("El Arco debería estar",true, contains2(g2.darArcos(), g2.darArcosOrigen("6")[0]));
	}
	
	public void testEliminarNodo1(){
		setupEscenario1();
		assertEquals("El nodo debería estar",true, contains(g1.darNodos(), new Nodo<Integer>(1)));
		g1.eliminarNodo(1);
		assertEquals("El nodo no debería estar",false, contains(g1.darNodos(), new Nodo<Integer>(1)));
	}
	public void testEliminarNodo2(){
		setupEscenario2();
		assertEquals("El nodo debería estar",true, contains(g2.darNodos(), new Nodo<String>("1")));
		g2.eliminarNodo("1");
		assertEquals("El nodo no debería estar",false, contains(g2.darNodos(), new Nodo<String>("1")));
	}
	public void testBuscarNodo1(){
		setupEscenario1();
		Nodo<Integer> nodo = new Nodo<Integer>(1);
		Nodo<Integer> nodo2 = g1.buscarNodo(1);
		assertEquals("Los nodos deberían ser iguales", true, nodo.equals(nodo2) );
	}
	public void testBuscarNodo2(){
		setupEscenario2();
		Nodo<String> nodo = new Nodo<String>("1");
		Nodo<String> nodo2 = g2.buscarNodo("1");
		assertEquals("Los nodos deberían ser iguales", true, nodo.equals(nodo2) );
	}
	
	
}
