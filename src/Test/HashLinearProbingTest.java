package Test;

import java.util.Comparator;
import java.util.Iterator;

import Estructuras.HashLinearProbing;
import junit.framework.TestCase;

public class HashLinearProbingTest extends TestCase {
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase donde se har�n las pruebas.
	 */
	private HashLinearProbing<String, Integer> tabla1;
	
	private HashLinearProbing<Integer, String> tabla2;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Escenario 1: Construye una nueva tabla con llaves de Strings, capacidad 5 y factor de carga m�xima 0.5
	 */
	private void setupEscenario1() 
	{
		tabla1 = new HashLinearProbing<String, Integer>(5, 0.5f);
	}

	/**
	 * Escenario 2: Construye una nueva tabla con llaves de enteros, capacidad 5 y factor de carga m�xima 0.5
	 */
	private void setupEscenario2() 
	{
		tabla2 = new HashLinearProbing<Integer, String>(5, 0.5f);
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo agregar.<br>
	 * <b> M�todos a probar:</b> <br>
	 * put<br>
	 * get<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La tabla est� vac�a. <br>
	 * 2. Se agregan elementos
	 */
	public void testAgregar() {
		setupEscenario1();
		assertEquals("No deber�a encontrar nada", null , tabla1.get("a"));
		tabla1.put("a", 1);
		tabla1.put("c", 2);
		assertEquals("El tama�o de la lista deber�a ser 2", 2, tabla1.size());
		assertNotNull("deber�a encontrar el objeto", tabla1.get("a"));
	
		setupEscenario2();
		assertEquals("No deber�an haber elementos", 0, tabla2.size());
		tabla2.put(1, "a");
		tabla2.put(2, "b");
		assertEquals("La capacidad deber�a seguir siendo 5", 5, tabla2.darCapacidad());
		assertEquals("No deber�a encontrar nada", null , tabla2.get(3));
		tabla2.put(3, "c");
		tabla2.put(4, "d");
		assertEquals("La capacidad deber�a haber aumentado al doble", 10, tabla2.darCapacidad());
		tabla2.put(5, "e");
		assertEquals("El numero de elementos deber�a aumentar", 5, tabla2.size());
		assertNotNull("deber�a encontrar el objeto", tabla2.get(3));
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo delete de la clase.<br>
	 * <b> M�todos a probar:</b> <br>
	 * delete<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. Se agregan y se quitan elementos.
	 */
	public void testEliminar() 
	{
		setupEscenario1();
		tabla1.put("a", 1);
		tabla1.put("c", 2);
		tabla1.put("e", 3);
		tabla1.put("g", 4);
		tabla1.put("i", 5);
		tabla1.put("k", 6);
		assertEquals("El numero de elementos deber�a ser 6", 6, tabla1.size());
		assertEquals("Deber�a encontrar el objeto correcto", 1 ,(int) tabla1.delete("a"));
		assertNull("No deber�a encontrar el objeto", tabla1.delete("a"));
		assertEquals("El numero de elementos deber�a disminuir", 5, tabla1.size());
		assertEquals("Deber�a encontrar el objeto correcto", 6, (int) tabla1.delete("k"));
		assertEquals("Deber�a encontrar el objeto correcto", 5, (int) tabla1.delete("i"));
		assertEquals("Deber�a encontrar el objeto correcto", 4, (int) tabla1.delete("g"));
		assertEquals("El numero de elementos deber�a disminuir", 2, tabla1.size());
		assertNull("No deber�a encontrar el objeto", tabla1.delete("i"));
		assertEquals("Deber�a encontrar el objeto correcto", 2, (int) tabla1.delete("c"));
		assertEquals("La capacidad deber�a disminuir", 5, tabla1.darCapacidad());
	
		setupEscenario2();
		tabla2.put(1, "a");
		tabla2.put(2, "b");
		tabla2.put(3, "c");
		tabla2.put(4, "d");
		tabla2.put(5, "e");
		tabla2.put(6, "f");
		assertEquals("El numero de elementos deber�a ser 6", 6, tabla2.size());
		assertEquals("Deber�a encontrar el objeto correcto", "a", (String) tabla2.delete(1));
		assertNull("No deber�a encontrar el objeto", tabla2.delete(1));
		assertEquals("El numero de elementos deber�a disminuir", 5, tabla2.size());
		assertEquals("Deber�a encontrar el objeto correcto","f",(String) tabla2.delete(6));
		assertEquals("Deber�a encontrar el objeto correcto","c",(String)tabla2.delete(3));
		assertEquals("Deber�a encontrar el objeto correcto","d",(String) tabla2.delete(4));
		assertEquals("El numero de elementos deber�a disminuir", 2, tabla2.size());
		assertNull("No deber�a encontrar el objeto", tabla2.delete(3));
		assertEquals("Deber�a encontrar el objeto correcto","b", (String) tabla2.delete(2));
		assertEquals("La capacidad deber�a disminuir", 5, tabla2.darCapacidad());
	}
}