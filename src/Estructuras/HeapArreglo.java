package Estructuras;

import java.util.Comparator;

public class HeapArreglo<T> {

	private int tam;

	private T[] laLista;

	private Comparator<T> comprador;

	public HeapArreglo(Comparator<T> c){
		laLista = (T[] )new Object[10];
		tam=0;
		comprador=c;
	}
	public HeapArreglo(int size, Comparator<T> c){
		laLista = (T[] )new Object[size];
		tam=0;
		comprador=c;
	}

	public void put(T elemento) {
		if(tam==laLista.length-1){
			resize();
		}
		laLista[tam]=elemento;

		if(tam>0){
			siftUp();
		}
		tam++;
	}

	public T peek() {
		return laLista[0];
	}

	public T poll() {
		T rta =laLista[0];
		laLista[0]=laLista[tam-1];
		laLista[tam-1]=null;
		tam--;
		siftDown();
		return rta;		
	}

	public int size() {
		// TODO Auto-generated method stub
		return tam;
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tam==0;
	}

	public void siftUp() {
		int indice = tam;
		int padre = (tam-1)/2;
		while(indice>0 && comprador.compare(laLista[indice], laLista[padre])<0){			
			swap(indice, padre);
			indice = padre;
			padre=indice/2;
		}

	}

	public void siftDown() {
		int pos =0;
		int hijo1 = 1;
		int hijo2 =2;
		boolean salio = false;
		while(!salio && hijo1<tam){
			if(hijo2>=tam){
				if(comprador.compare(laLista[pos], laLista[hijo1])<=0){					
					salio=true;
				}
				else{
					swap(pos, hijo1);
					hijo1 = (pos*2)+1;
					hijo2 = (pos*2)+2;
				}
			}
			else{
				int hijoMayor = hijo1;
				int n =comprador.compare(laLista[hijoMayor], laLista[hijo2]);
				if(n>0){					
					hijoMayor=hijo2;
				}
				n = comprador.compare(laLista[pos], laLista[hijoMayor]);
				if(n<=0){					
					salio = true;
				}
				else{
					swap(pos, hijoMayor);
					pos=hijoMayor;
					hijo1 = (pos*2)+1;
					hijo2 = (pos*2)+2;
				}
			}
		}

	}

	public void swap(int e1, int e2){
		try{
			T temp = laLista[e1];
			laLista[e1] = laLista[e2];
			laLista[e2] = temp;
		}
		catch (Exception e){

		}
	}
	public void resize(){
		int nTam = 2*tam;
		T[] lista2 = (T[]) new Object[nTam]; 
		for(int i =0; i<tam; i++){
			lista2[i]=laLista[i];
		}
		laLista = lista2;
	}
}
