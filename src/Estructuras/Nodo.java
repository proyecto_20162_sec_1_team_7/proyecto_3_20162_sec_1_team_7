package Estructuras;

/**
 * Interface que representa un nodo de un grafo.
 * @param K tipo del identificador de los vertices
 *
 */
public class Nodo<K extends Comparable<K>> implements Comparable<Nodo<K>> {
	
	/**
	 * Identificador único del nodo
	 * @return id
	 */
	public K Id;
	
	public Nodo(K data){
		Id=data;
	}
	
	public K darId(){
		return Id;
	}
	
	public boolean equals(Nodo<K> nodo){
		return Id.equals(nodo.darId());
	}

	public int compareTo(Nodo<K> arg0) {
		return Id.compareTo(arg0.darId());
	}
	
	

}
