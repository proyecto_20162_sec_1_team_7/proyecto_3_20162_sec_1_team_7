package Estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private Lista<Nodo<K>> nodos;

	/**
	 * Lista de adyacencia 
	 */
	private HashLinearProbing<K, Lista<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		nodos = new Lista<Nodo<K>>();
		adj = new HashLinearProbing<K, Lista<Arco<K,E>>>();
	}

	
	public boolean agregarNodo(Nodo<K> nodo) {
		if(!nodos.contains(nodo)){
			nodos.agregarAlFinal(nodo);
			adj.put(nodo.darId(), new Lista<Arco<K,E>>());
			return true;
		}
		return false;
	}
	public boolean agregarNodo(K id){
		Lista<Arco<K,E>> l = adj.get(id);
		if(l==null){
			Nodo<K> nodo = new Nodo<K>(id);
			nodos.agregarAlFinal(nodo);
			adj.put(nodo.darId(), new Lista<Arco<K,E>>());
			return true;
		}
		return false;
	}

	public boolean eliminarNodo(K id) {
		adj.delete(id);
		if(adj==null){
			return false;
		}
		else{
			Iterator<Nodo<K>> i = nodos.iterator();
			while (i.hasNext()){
				Nodo<K> actual = i.next();
				K data = actual.darId();
				if(data.equals(id)){
					nodos.remove(actual);
					return true;
				}
			}
		}
		return false;
	}

	
	public Arco<K,E>[] darArcos() {
		Lista<Arco<K,E>> arcos = new Lista<>();
		Lista<Nodo<K>> visitados = new Lista<>();
		Iterator<Nodo<K>> i = nodos.iterator();
		while(i.hasNext()){
			Nodo<K> actual = i.next();
			Lista<Arco<K, E>> actuales = adj.get(actual.darId());
			Iterator<Arco<K, E>> j = actuales.iterator();
			while(j.hasNext()){
				Arco arc = j.next();
				Nodo<K> inicio =arc.darNodoInicio(); 
				Nodo<K> fin =arc.darNodoFin();
				if(!(visitados.contains(inicio) && visitados.contains(fin))){
					arcos.agregarAlFinal(arc);
				}
			}
		}
		Arco<K,E>[] rta = (Arco<K,E>[])new Arco[arcos.size()];
		Iterator<Arco<K, E>> i2 = arcos.iterator();
		for(int m =0; i2.hasNext();m++){
			rta[m]=i2.next();
		}
		return rta;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		Iterator<Nodo<K>> i= nodos.iterator();
		Nodo<K>[] rta = (Nodo<K>[]) new Nodo[nodos.size()];
		int n=0;
		while(i.hasNext()){
			rta[n]=i.next();
			n++;
		}
		return rta;
	}

	@Override
	public boolean agregarArco(K pInicio, K pFin, double pCosto, E pObj) {
		Nodo<K> inicio = buscarNodo(pInicio);
		Nodo<K> fin = buscarNodo(pFin);
		if(inicio!=null && fin!=null){
			Arco<K, E> arc = new Arco<>(inicio, fin, pCosto, pObj);
			adj.get(inicio.darId()).agregarAlFinal(arc);
			adj.get(fin.darId()).agregarAlFinal(arc);
			return true;
		}
		return false;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	public Arco<K,E> eliminarArco(K pInicio, K pFin) {
		Nodo<K> inicio = buscarNodo(pInicio);
		Nodo<K> fin = buscarNodo(pFin);
		Lista<Arco<K, E>> adji = adj.get(inicio.darId());
		Lista<Arco<K, E>> adjf = adj.get(fin.darId());
		Iterator<Arco<K, E>> i = adji.iterator();
		Arco<K,E> rta = null;
		while(i.hasNext()){
			Arco<K, E> actual =i.next();
			if(actual.darNodoInicio().equals(inicio) && actual.darNodoFin().equals(fin) ){
				rta = actual;
				break;
			}			
		}
		if(rta !=null){
			adji.remove(rta);
			adjf.remove(rta);
		}
		return rta;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		Iterator<Nodo<K>> i = nodos.iterator();
		while(i.hasNext()){
			Nodo<K> actual = i.next();
			if(actual.darId().equals(id)){
				return actual;
			}
		}
		return null;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		Lista<Arco<K, E>> arcos = adj.get(id);
		Lista<Arco<K, E>> rta1 = new Lista<Arco<K, E>>();
		Iterator<Arco<K, E>> i = arcos.iterator();
		while (i.hasNext()) {
			Arco<K,E>	actual = i.next();
			if(actual.darNodoInicio().darId().equals(id)){
				rta1.agregarAlFinal(actual);
			}
		}
		Arco<K, E>[] rta = (Arco<K, E>[]) new Arco[rta1.size()];
		i = rta1.iterator();
		int n =0;
		while(i.hasNext()){
			rta[n] = i.next();
			n++;
		}
		return rta;

	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		Lista<Arco<K, E>> arcos = adj.get(id);
		Lista<Arco<K, E>> rta1 = new Lista<Arco<K, E>>();
		Iterator<Arco<K, E>> i = arcos.iterator();
		while (i.hasNext()) {
			Arco<K,E>	actual = i.next();
			if(actual.darNodoFin().darId().equals(id)){
				rta1.agregarAlFinal(actual);
			}
		}
		Arco<K, E>[] rta = (Arco<K, E>[]) new Arco[rta1.size()];
		i = rta1.iterator();
		int n =0;
		while(i.hasNext()){
			rta[n] = i.next();
			n++;
		}
		return rta;		
	}

	public Lista<Arco<K, E>> darArcosPasan(K id){
		return adj.get(id);
	}

}
