package mundo;

import java.io.*;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import Estructuras.*;


public class Catalogo 
{



	public Catalogo()
	{

	}	

	public void cargarVuelos(String datos) 
	{
		FileInputStream fis= null;
		try
		{
			fis=new FileInputStream(new File(datos));
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet vuelos = wb.getSheetAt(0);
			//Iterate through each rows one by one
			Iterator<Row> rowIterator = vuelos.iterator();
			String tuple= "";
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getRowNum()==0 || row.getRowNum()==1) continue;
				//Ayudita para christian. El �ltimo valor de la columna para saber cuando termin� de leer un vuelo completo
				System.out.println(vuelos.getRow(0).getCell(row.getLastCellNum()-1).getStringCellValue()+" "+ vuelos.getRow(1).getCell(row.getLastCellNum()-1).getStringCellValue());
				//For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch(vuelos.getRow(0).getCell(cell.getColumnIndex()).getStringCellValue()+" "+vuelos.getRow(1).getCell(cell.getColumnIndex()).getStringCellValue())
					{
					case "Nombre ":
						System.out.println("Aerolinea: "+cell.getStringCellValue());
						break;
					case "N�mero de Vuelo":
						System.out.println("N�mero de vuelo: "+cell.getNumericCellValue());
						break;
					case "Origen ":
						System.out.println("Origen: "+cell.getStringCellValue());
						break;
					case "Destino ":
						System.out.println("Destino: "+cell.getStringCellValue());
						break;
					case "Hora de Salida":
						System.out.println("Hora de salida: "+cell.getNumericCellValue());
						break;
					case "Hora de Llegada":
						System.out.println("Hora de llegada: "+cell.getNumericCellValue());
						break;
					case "Tipo Equipo":
						System.out.println("Tipo de equipo: "+cell.getStringCellValue());
						break;
					case "N�mero de Sillas":
						System.out.println("Num de sillas: "+cell.getNumericCellValue());
						break;
					case "Lu ":
						System.out.println("Lunes: "+cell.getStringCellValue());
						break;
					case "Ma ":
						System.out.println("Martes: "+cell.getStringCellValue());
						break;
					case "Mi ":
						System.out.println("Miercoles: "+cell.getStringCellValue());
						break;
					case "Ju ":
						System.out.println("Jueves: "+cell.getStringCellValue());
						break;
					case "Vi ":
						System.out.println("Viernes: "+cell.getStringCellValue());
						break;
					case "Sa ":
						System.out.println("S�bado: "+cell.getStringCellValue());
						break;
					case "Do ":
						System.out.println("Domingo: "+cell.getStringCellValue());
						break;
					case "Tipo Vuelo":
						System.out.println("Nacional o internacional: "+cell.getStringCellValue());
						break;
					default:
						System.out.println("caso default: "+vuelos.getRow(0).getCell(cell.getColumnIndex()).getStringCellValue()+" "+vuelos.getRow(1).getCell(cell.getColumnIndex()).getStringCellValue());
						break;
					}
				}
				System.out.println(vuelos.getRow(0).getCell(row.getLastCellNum()-1).getStringCellValue()+ " "+ vuelos.getRow(1).getCell(row.getLastCellNum()-1).getStringCellValue());
			}
		}    
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {

				try {
					fis.close();
					fis = null;
				} catch (Exception e) {

					System.out.println("File closing operation failed");
					e.printStackTrace();
				}
			}                                 

		}
	}

	public void cargarTarifa(String datos)
	{
		FileInputStream fis= null;
		try
		{
			fis=new FileInputStream(new File(datos));
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet vuelos = wb.getSheetAt(1);
			//Iterate through each rows one by one
			Iterator<Row> rowIterator = vuelos.iterator();
			String tuple= "";
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getRowNum()==0) continue;
				//Ayudita para christian. El �ltimo valor de la columna para saber cuando termin� de leer un vuelo completo
				System.out.println(vuelos.getRow(0).getCell(row.getLastCellNum()-1).getStringCellValue());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch(vuelos.getRow(0).getCell(cell.getColumnIndex()).getStringCellValue())
					{
					case "AEROL�NEA":
						System.out.println("Aerolinea: "+cell.getStringCellValue());
						break;
					case "VALOR MINUTO":
						System.out.println("Valor por minuto: "+cell.getStringCellValue());
						break;
					case "NUM SILLAS PARA VALOR MAX POR MINUTO":
						System.out.println("Num sillas para valor max: "+cell.getNumericCellValue());
						break;
					default:
						System.out.println("caso default: "+vuelos.getRow(0).getCell(cell.getColumnIndex()).getStringCellValue()+" "+vuelos.getRow(1).getCell(cell.getColumnIndex()).getStringCellValue());
						break;
					}
				}
				System.out.println(vuelos.getRow(0).getCell(row.getLastCellNum()-1).getStringCellValue());
			}
		}    
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {

				try {
					fis.close();
					fis = null;
				} catch (Exception e) {

					System.out.println("File closing operation failed");
					e.printStackTrace();
				}
			}                                 

		}
	}
}